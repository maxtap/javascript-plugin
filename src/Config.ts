import { Config } from "./types"
const config: Config = {
    GoogleAnalyticsCode: 'G-05P2385Q2K',
    MaxTapComponentElementId: 'componentmaxtap',
    MaxTapMainContainerId: 'containermaxtap',
    DataAttribute: 'data-displaymaxtap',
    classNames: {
        image_wrapper: "maxtap_img_wrapper",
    },
    PrefetchImageTime: 15,
    DataUrl: "https://storage.googleapis.com/publicmaxtap-prod.appspot.com/data"
}
export default config;